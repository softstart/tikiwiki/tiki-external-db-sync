#!/bin/bash

if [ ! -f .setup ]; then
    sleep 30
    bin/symadmin --engine corp-000 create-sym-tables
    sleep 5
    bin/dbimport --engine corp-000 samples/insert_sample.sql
    touch .setup
    sleep 5
fi


