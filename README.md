# TIKI  External DB Sync
The main goal of this project is to prove that is possible to syncronize a My SQL database to PostgreSQL and Microsoft SQL databases.

## Architecture
This docker-compose based POC is ready with 5 images:

* Tiki Wiki instance
* MySql: Main database
* SymmetricDS: tool that allows to replicate multiple databases near real time and incrementally.
* PostgreSQL: Slave database that will be a replica of the master database
* MS SQL: Slave database that will be a replica of the master database


## How to use
1. Run `docker-compose up -V` and wait a few minutes until all schemes are syncronized.
2. Open [Tiki](http://localhost:8080) and login with username `admin` and password `admin`.
3. Make a change in Tiki and check if the other databases have the lastest data.

## Known issues
This Tiki´s database version only has primary keys, the other types of indexes were removed due to the lack of compatibility with the other databases systems. Later we can overpass this problem by giving different names to each index/key.


